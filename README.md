# Old school mix-tape or cd project

A little python project to create old school mix-CDs out of an m3u playlist

## Dependencies
* m3u8
* music-tag
* urllib.parse
## Install
```bash
pip install m3u8
pip install music-tag
```
## TODO
* external cd cover

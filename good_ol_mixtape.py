#!/usr/bin/python3
# -*- coding: UTF-8 -*-
# created by lzbk
import m3u8
import music_tag
from urllib.parse import unquote
import os

class Song:
	def __init__(self,song_path, mixAlbum = None, mixNumber = None, mixAuthor=None, mixCover=None):
		self.path = song_path
		self.track = music_tag.load_file(self.path)
		self.title = self.track['title'].first
		self.o_album = self.track['album'].first
		self.artist = self.track['artist'].first
		self.o_number = self.track['tracknumber'].first
		self.o_year = self.track['year'].first
		if mixAlbum != None:
			self.set_mixTitle(mixAlbum)
		if mixNumber != None:
			self.set_numInMix(mixNumber)
		if mixAuthor != None:
			self.set_albumArtist(mixAuthor)
		if mixCover != None:
			self.replace_artwork(mixCover)

	def set_mixTitle(self, mixAlbum):
		self.track['album']=mixAlbum

	def set_numInMix(self, mixNumber):
		self.track['tracknumber']=mixNumber

	def set_totalnum(self, totalNumber):
		self.track['totaltracks']=totalNumber

	def set_albumArtist(self, albumArtist):
		self.track['albumartist'] = albumArtist
		self.track['compilation'] = True

	def replace_artwork(self, newImagePath):
		tmpArt = []
		for a in self.track['artwork'].values:
			tmpArt.append(a)
		self.track.remove_tag('artwork')
		with open(newImagePath, 'rb') as img_in:
			self.track.append_tag('artwork', img_in.read())
		for a in tmpArt:
			self.track.append_tag('artwork', a)

	def update_comment(self):
		self.track.append_tag('comment', f"Track #{self.o_number} from {self.o_year} album “{self.o_album}”.")

	def save_tag(self):
		self.track.save()
		print("uncomment to update "+str(self))

	def rename_track(self):
		self.save_tag()
		new_path = f"{os.path.dirname(self.path)}/{self.track['tracknumber'].first}. {self.title} [{self.artist}]{os.path.splitext(os.path.basename(self.path))[1]}"
		os.rename(self.path, new_path)
		self.path = new_path
		self.track = music_tag.load_file(self.path)


	def __str__(self):
		return f"{self.track['tracknumber'].first}/{self.track['totaltracks'].first}. {self.track['album'].first}/{self.title} — {self.artist} (track #{self.o_number} of {self.o_album})"

class MixTape:
	@classmethod
	def uri2path(cls, uri):
		res = uri.replace("file://","")
		if res == uri:
			raise ValueError(uri+" is not a file uri.")
		else:
			return unquote(res)

	def __init__(self, pl_path, title = None, author = None, img = None):
		self.playlist = []
		self.title = title
		self.author = author
		self.cover = img
		num = 0
		for s in m3u8.load(pl_path).segments:
			try:
				if self.title == None :
					self.playlist.append(Song(MixTape.uri2path(s.uri)))
				else:
					num += 1
					self.playlist.append(Song(MixTape.uri2path(s.uri), self.title, num, self.author, self.cover))
			except ValueError as e:
				print(e)
				num -= 1

	def update_tags(self):
		for t in self.playlist:
			t.set_totalnum(len(self.playlist))
			t.update_comment()
			t.save_tag()
			t.rename_track()

	def __str__(self):
		res = ""
		for track in self.playlist:
			res += track.__str__()+"\n"
		return res



if __name__ == "__main__":
	s = Song("sampleData/archive/02. NoName - Rainforest.flac", "test", 3, "me", "sampleData/CONNECTED.png")
	s.set_totalnum(5)
	s.update_comment()
	s.rename_track()
	s.save_tag()

#!/usr/bin/python3
# -*- coding: UTF-8 -*-
# created by lzbk

import argparse
from good_ol_mixtape import MixTape
parser = argparse.ArgumentParser(description="Créer une mix-tape")
parser.add_argument("-p", "--playlist",  help="le fichier .m3u8 de la playlist", type=str)
parser.add_argument("-t", "--mix_title",  help="le titre du mix", type=str, default = None)
parser.add_argument("-a", "--mix_author",  help="l'auteur du mix", type=str, default = None)
parser.add_argument("-i", "--mix_image",  help="la couverture du mix", type=str, default = None)
args = parser.parse_args()

if __name__ == "__main__":
	mx = MixTape(args.playlist, args.mix_title, args.mix_author, args.mix_image)
	mx.update_tags()
	print(mx)
